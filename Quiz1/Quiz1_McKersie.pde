void setup()
{
 size (1280, 720, P3D);
 camera (0,0,-(height/2.0) / tan(PI*30.0 / 180.0),0,0,0,0,-1,0);
}
void draw()
{
  background(0);
  
  
  drawCartesianPlane();
  drawQuadraticFunction();
  drawLinearFunction();
  drawSineWave();
}
void drawCartesianPlane()
{
  strokeWeight(1);
  color white =  color(255,255,255);
  fill(white);
  stroke(white);
  line(300,0,-300,0);
  line(0,-300,0,300);
  for (int x = -300; x <= 300; x += 10){
  line(x,-2,x,2);
  line(-2,x,2,x);
  }
}

void drawQuadraticFunction()
{
  color yellow = color(255,255,0);
  fill(yellow);
  stroke(yellow);
  noStroke();
  for(float x = -300; x <= 300; x += 0.1f)
  {
    circle(x,(float)Math.pow(x,2) - (15*x) - 3, 5);
  }
}

void drawLinearFunction()
{
  color purple = color(255,0,255);
  fill(purple);
  stroke(purple);
  noStroke();
  for(int x = -300; x <= 300; x++)
  {
    circle(x,((-5)*x)+30,5);
  }
  
}

void drawSineWave()
{
  color blue = color(0,0,255);
  fill(blue);
  stroke(blue);
 
 float x = -1000;
 while(x<width){
   point(x,0+40*sin(x/10));
   x=x+1;
 }
}

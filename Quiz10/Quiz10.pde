
Walker[] walkers = new Walker[10];
Liquid ocean = new Liquid(0,-100,Window.right,Window.bottom,0.1f);

PVector gravity = new PVector(0, -0.4);
PVector wind = new PVector(0.15, 0);

void setup()
{
  size(1280, 720, P3D);
  camera(0,0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
 for (int i = 0; i < 10; i++)
 {
   float posX = 2 * (Window.windowWidth / 10) * (i - 5);
   
   walkers[i] = new Walker();
   walkers[i].position = new PVector(posX,300);
   walkers[i].mass = random (20,50);
   walkers[i].scale = walkers[i].mass * 2;
    
   walkers[i].r = random(255);
   walkers[i].g = random(255);
   walkers[i].b = random(255);
   walkers[i].a = random(150,255);
    
 }
  
}



void draw()
{
 background(80); 
 ocean.render();
 for(Walker walker : walkers)
 {
   
   float mew = 0.1f;
   float normal = 1;
   float frictionMagnitude = mew * normal;
   PVector friction = walker.velocity.copy();
   
   walker.applyForce(friction.mult(-1).normalize().mult(frictionMagnitude));
   
   walker.render();
   walker.update();
   noStroke();
   
 PVector wind = new PVector (0.1,0);
 PVector gravity = new PVector(0, -0.15 * walker.mass);
 walker.applyForce(gravity);
 walker.applyForce(wind);
 
 
 if (walker.position.y <= Window.bottom)
 {
 walker.position.y = Window.bottom;
 walker.velocity.y *= -1;
 }
 if (ocean.isCollidingWith(walker))
 {
  PVector dragForce = ocean.calculateDragForce(walker);
  walker.applyForce(dragForce);
 }
 
 
 }

 
}

Walker[] walkers = new Walker[10];


PVector gravity = new PVector(0, -0.4);
PVector wind = new PVector(0.15, 0);

void setup()
{
  size(1280, 720, P3D);
  camera(0,0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  
  

}

void draw()
{
  background(255);
   for (int i = 0; i < 5; i++)
 {
   
   walkers[i] = new Walker();
   walkers[i].position.x = random (-1280,1280);
   walkers[i].position.y = random (-720,720);
   walkers[i].mass = random (20,50);
   walkers[i].scale = walkers[i].mass;
    
   walkers[i].r = random(255);
   walkers[i].g = random(255);
   walkers[i].b = random(255);
   walkers[i].a = random(150,255);
 
    for (int j = 0; j > 5 ; j--)
    {
      
   walkers[j] = new Walker();
   walkers[j].position.x = random (-1280,1280);
   walkers[j].position.y = random (-720,720);
   walkers[j].mass = random (20,50);
   walkers[j].scale = walkers[i].mass;
    
   walkers[j].r = random(255);
   walkers[j].g = random(255);
   walkers[j].b = random(255);
   walkers[j].a = random(150,255);
    
    
    if(i != j)
     {  
      walkers[i].applyForce(walkers[j].calculateAttraction(walkers[i]));
      
    }
    }
 }
 
  for(Walker walker : walkers)
 {
   
  
  walker.render();
  
  walker.update();
  noStroke();
  
  
 }
  
  
  
}

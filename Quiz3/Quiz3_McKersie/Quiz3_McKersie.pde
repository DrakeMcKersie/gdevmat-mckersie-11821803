void setup()
{
 size (1020, 720, P3D);
 camera (0,0,-(height/2.0) / tan(PI*30.0 / 180.0),0,0,0,0,-1,0);
 background(0);
}
void draw()
{

  
float gaussian = randomGaussian();
float standardDeviation = 500;
float mean = 100;
float a = standardDeviation * gaussian + mean;
float gaussian2 = randomGaussian();
float standardDeviation2 = random(100);
float mean2 = random(20);
float b = standardDeviation2 * gaussian2 + mean2;


  fill(random(255),random(255),random(255),random(10,100));
  noStroke();
  circle(a, random(-360, 360), b);
  
}


  

class Walker
{
  public float x;
  public float y;
  public float a;
  public float r;
  public float g;
  public float b;
  public float tx = 0, ty = 10000,ta = 0, tr = 255,tg = 255, tb = 255;

  void render()
  {
    fill(r,g,b);
    noStroke();
    circle(x,y,a);
  }
  
  void perlinWalk()
  {
    
    x = map(noise(tx), 0, 1, -640, 640);
    y = map(noise(ty), 0, 1, -360, 360);
    a = map(noise(ta),0, 1, 5, 150);
  
    
    tx += 0.01f;
    ty += 0.01f;
    ta += 0.01f;
    
  } 
    
     void perlinColor()
  {
    
  
    r = map(noise(tr),0, 1, 0, 255);
    g = map(noise(tg),0, 1, 0, 255);
    b = map(noise(tb),0, 1, 0, 255);
    
    tr += 0.06f;
    tg += 0.04f;
    tb += 0.05f;
    
  } 
    
  }

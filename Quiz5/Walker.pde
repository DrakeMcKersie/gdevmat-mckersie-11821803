class Walker
{
  
PVector walkPosition = new PVector(); 

  void randomWalk()
  {
    int rng = int(random(8));
    
    if (rng == 0)
    {
      walkPosition.y += 10;
    }
   else if (rng == 1)
    {
      walkPosition.y -= 10;
    }
   else if (rng == 2)
    {
      walkPosition.x += 10;
    }
   else if(rng == 3)
   {
     walkPosition.x -= 10;
   }
   else if(rng == 4)
   {
     walkPosition.y+= 10;
     walkPosition.x+= 10;
   }
   else if(rng == 5)
   {
     walkPosition.y+= 10;
     walkPosition.x-= 10;
   }
   else if(rng == 6)
   {
     walkPosition.y-= 10;
     walkPosition.x+= 10;
   }
   else if(rng == 7)
   {
     walkPosition.y-= 10;
     walkPosition.x-= 10;
   }
   
    fill(random(255),random(255),random(255),random(50,100));
    noStroke();
    circle(walkPosition.x,walkPosition.y,30);
  }
  
  PVector walkPositionb = new PVector();

void randomWalkBiased()
  {
     float rng = (random(1));
    
    if (rng < 0.1)
    {
      walkPositionb.y += 10;
    }
   else if (rng < .2)
    {
      walkPositionb.y -= 10;
    }
   else if (rng < .3)
    {
      walkPositionb.x += 10;
    }
   else if(rng < .4)
   {
     walkPositionb.x -= 10;
   }
   else if(rng < .5)
   {
     walkPositionb.y+= 10;
     walkPositionb.x+= 10;
   }
   else if(rng < .6)
   {
     walkPositionb.y+= 10;
     walkPositionb.x-= 10;
   }
   else if(rng < .7)
   {
     walkPositionb.y-= 10;
     walkPositionb.x+= 10;
   }
   else if(rng < .8)
   {
     walkPositionb.y-= 10;
     walkPositionb.x-= 10;
   }
    fill(random(255),random(255),random(255),random(50,100));
    noStroke();
    circle(walkPositionb.x,walkPositionb.y,30);
  }

PVector position = new PVector();
PVector speed = new PVector(5,8);

void moveAndBounce()
{
  position.add(speed);
  
  if (( position.x>Window.right) || ( position.x < Window.left))
  {
    speed.x *= -1;
  }
  if (( position.y > Window.top) || ( position.y < Window.bottom))
  {
    speed.y *= -1;
  }
  
 fill(182,52,100);
 circle( position.x, position.y,50);

}
}


Walker[] walkers = new Walker[10];
void setup()
{
  size(1280, 720, P3D);
  camera(0,0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
   for (int i = 0; i <10; i++)
 {
  PVector mouse = mousePos();
   
  
   walkers[i] = new Walker();
   walkers[i].position = new PVector(random(-720,720),random (-1280,1280));
   walkers[i].scale = random(10,40);
   walkers[i].acceleration = new PVector(mouse.x, mouse.y);
   walkers[i].direction = PVector.sub (mouse, walkers[i].position);
 }
  
}


PVector mousePos()
{
  float x = mouseX - Window.windowWidth / 2;
  float y = -(mouseY - Window.windowHeight / 2);
  return new PVector(x,y);
  

}


void draw()
{
 background(80); 
 
  for(Walker walker : walkers)
 {
 
 
 walker.update();
 walker.render();
 walker.checkEdges();

 

 

}
}

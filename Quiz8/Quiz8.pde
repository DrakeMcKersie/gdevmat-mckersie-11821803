
Walker[] walkers = new Walker[10];

PVector gravity = new PVector(0, -0.4);
PVector wind = new PVector(0.15, 0);

void setup()
{
  size(1280, 720, P3D);
  camera(0,0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
 for (int i = 0; i <10; i++)
 {
   walkers[i] = new Walker();
   walkers[i].position = new PVector(-500,200);
   walkers[i].mass = 10 - i;
   walkers[i].scale = walkers[i].mass*15;
   walkers[i].r = random(255);
   walkers[i].g = random(255);
   walkers[i].b = random(255);
   walkers[i].a = random(150,255);
    
 }
  
}




void draw()
{
 background(80); 
 noStroke();
 for(Walker walker : walkers)
 {

 walker.update();
 walker.render();
 walker.applyForce(wind);
 walker.applyForce(gravity);
 
 if(walker.position.x >= Window.right)
 {
   walker.position.x = Window.right;
   walker.velocity.x *= -1;
   
 }
 if(walker.position.y <= Window.bottom)
 {
   walker.position.y = Window.bottom;
  walker.velocity.y *= -1; 
 }
 

}
}

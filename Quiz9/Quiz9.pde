
Walker[] walkers = new Walker[8];

PVector gravity = new PVector(0, -0.4);
PVector wind = new PVector(0.15, 0);

void setup()
{
  size(1280, 720, P3D);
  camera(0,0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
 for (int i = 0; i <8; i++)
 {
   //float posX = 2 * (Window.windowWidth / 10) * (i - 5);
   float posY = 2 *(Window.windowHeight / 8)  * (i - (8 / 2));
   walkers[i] = new Walker();
   walkers[i].position = new PVector(-500,posY);
   walkers[i].mass = 10 - i;
   walkers[i].scale = walkers[i].mass*15;
    walkers[i].acceleration = new PVector(0.2,0);
   walkers[i].r = random(255);
   walkers[i].g = random(255);
   walkers[i].b = random(255);
   walkers[i].a = random(150,255);
    
 }
  
}




void draw()
{
 background(80); 
 noStroke();
 for(Walker walker : walkers)
 {

   PVector acceleration = new PVector (0.2,0);
   
   float mew = 0.01f;
   float normal = 1;
   float frictionMagnitude = mew * normal;
   PVector friction = walker.velocity.copy();
   friction.mult(-1);
   friction.normalize();
   friction.mult(frictionMagnitude);
  
   
  //PVector gravity = new PVector(0, -0.15 * walker.mass);
 walker.update();
 walker.render();
 //walker.applyForce(wind);
 walker.applyForce(acceleration);
 walker.applyForce(friction);
 
 if(walker.position.x >= Window.right)
 {
   walker.position.x = Window.right;
   walker.velocity.x *= -1;
   
 }
 if(walker.position.y <= Window.bottom)
 {
   walker.position.y = Window.bottom;
  walker.velocity.y *= -1; 
 }
 if(walker.position.y == 0)
 {
   mew = 0.4f;
 }

}
}
